const l = require('../core/functions.js');
const moment = require("moment")
var Config = require('../config.js')
var config = new Config.ExchangesConfig()
const redis_get = require("redis").createClient(config.redis)
redis_get.on("error", function (err) {l.log("Error " + err)})
const redis_pub = require("redis").createClient(config.redis)
redis_pub.on("error", function (err) {l.log("Error " + err)})

class Signal{
    constructor(exchange,pair) {
        this.exchange=exchange
        this.pair=pair
        this.trade_hist_channel='converted.'+exchange+'.trade_hist.'+pair
        this.order_book_channel='converted.'+exchange+'.order_book.'+pair
        this.period=config.data[exchange].pairs[pair].period
        this.signal={vol:null,high:null,low:null,ask:null,bid:null,spread:null,over_bs:{buy_cnt:null,buy_vol:null,sell_cnt:null,sell_vol:null}}
        //каждую секунду общитываем
        setInterval(function (context) {context.getMarketStateSignal(context.trade_hist_channel,context.order_book_channel,context.period)},1000,this)
        //каждую секунду публикуем
        setInterval(function (context) {redis_pub.publish('signal.'+context.exchange+'.market_state.'+context.pair,JSON.stringify(context.signal))},1000,this)
    }

    getMarketStateSignal(){
        var promise_th = new Promise((resolve, reject) => {
// console.log(his.trade_hist_channel)
            redis_get.get(this.trade_hist_channel, function(err, data) {
              if (err) {l.log(err);return}
              if (data) {data = JSON.parse(data);resolve(data)} else {reject('not_found_data')}
            })
        }).then(data => {
            this.signal.vol=0
            this.signal.high=0
            this.signal.low=null
            this.signal.over_bs.buy_cnt=0
            this.signal.over_bs.sell_cnt=0
            this.signal.over_bs.buy_vol=0
            this.signal.over_bs.sell_vol=0
            let key
            for (key in data){
                if ((data[key].date+config.data[this.exchange].exchange_time_shift)>(moment().unix()-this.period)){
                    //посчитать объём
                    this.signal.vol=Number(this.signal.vol)+Number(data[key].amount)
                    //high low
                    if (this.signal.high<data[key].rate){this.signal.high=data[key].rate}
                    if (this.signal.low === null || this.signal.low>data[key].rate){this.signal.low=data[key].rate}
                    //over_bs
                    if (data[key].type === 'buy'){this.signal.over_bs.buy_cnt++;this.signal.over_bs.buy_vol=Number(this.signal.over_bs.buy_vol)+Number(data[key].amount)}
                    if (data[key].type === 'sell'){this.signal.over_bs.sell_cnt++;this.signal.over_bs.sell_vol=Number(this.signal.over_bs.sell_vol)+Number(data[key].amount)}
                }
            }
        },not_found_data => {l.log(this.trade_hist_channel+' Нет данных!')})

        var promise_ob = new Promise((resolve, reject) => {
// console.log(this.order_book_channel)
            redis_get.get(this.order_book_channel, function(err, data) {
// console.log(data)
                if (err) {l.log(err);return}
                if (data) {data=JSON.parse(data);resolve(data)} else {reject('not_found_data')}
            })
        }).then(data => {
// console.log(data)
                this.signal.ask=null
                this.signal.bid=0
                let key
                for (key in data.asks){if (this.signal.ask === null || this.signal.ask > +key){this.signal.ask= +key}}
                for (key in data.bids){if (this.signal.bid < +key){this.signal.bid = +key}}
                this.signal.spread= Number(this.signal.ask)-Number(this.signal.bid)
            },not_found_data => {l.log(this.order_book_channel+' Нет данных!')}
        )
    }
}

//запустить всё для всех
for (exchange in config.data){for (pair in config.data[exchange].pairs){new Signal(exchange,pair);l.log('Market State Signal для '+exchange+' '+pair)}}


