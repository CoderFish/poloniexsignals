const uid = require('uuid/v4')
const l = require('../core/functions.js')
const Orders = require('./order.js')
var Config = require('../config.js')
var config = new Config.ExchangesConfig()

const redis_sub = require("redis").createClient(config.redis)
redis_sub.on("error", function (err) {l.log("Error " + err)})

function Deal(params){
    this.id=uid()
    if (!params.order){return {}}
    this.exchange = params.exchange
    this.queue = params.queue
    this.status = {state:'open',comment:''}
    params.order.exchange = this.exchange
    params.order.queue = this.queue
    this.subscribeOnPair('signal.poloniex.market_state.'+params.order.pair)

    if (params.tp){this.tp = new Orders.Order({oparams:params.tp,deal:this})}
    if (params.sl){this.sl = new Orders.Order({oparams:params.sl,deal:this})}
    this.order = new Orders.Order({oparams:params.order,deal:this})

    this.order.putOrder()
    this.ABCState
    this.oldABCState=false
}

//подписались на сигналы с рынка bid,ask
Deal.prototype.subscribeOnPair = function (channel) {
    redis_sub.subscribe(channel)
    l.log('Сделка №'+this.id+'Подписался на канал: '+channel)
    redis_sub.on("message", function (channel, data) {
        if (!this.market_state){this.market_state={}}
        data = JSON.parse(data)
        this.market_state.ask=data.ask
        this.market_state.bid=data.bid
        //ToDo
        //ещё нужно контролить что данные актуальны
    }.bind(this))
}


//обработка действий по установке и снятию TP SL
Deal.prototype.processTPSL = function () {
    this.setABCState()
    //обработка ордера
    if (this.order.status === 'executed'){
        this.calcTPSL()
        //отреагировать на изменения
        this.order.status = 'executed_processed'
    }
    if (this.order.status === 'executed_part'){
        this.calcTPSL()
        //отреагироать на изменения
    }
    if (this.order.status === 'fail'){
        this.status.state = 'closed'
        this.status.comment = 'Не удалось поставить ордер'
    }
    if (this.order.status === 'canceled'){
        //если ордер исполнен частично
            //нужно сообщить в консоль
            //и непосредственно пользователю
            //закрыть сделку
    }
    //обработка tp
    // if (this.order.status === 'executed')
}

//обновляет состояние ABCState в модели
Deal.prototype.setABCState = function () {
    //      'a' когда заявки на продажу выше входа
    //      'b' когда заявка на продажу ниже входа
    //      'c' когда заявки на покупку ниже или равны стопу
    //      false защита от нулевых данных с подписки
    let result
    if (Number(this.order.rate)<Number(this.market_state.ask)){result = 'a'}
    if (Number(this.order.rate)>=Number(this.market_state.ask)){result = 'b'}
    if (this.sl !== undefined && Number(this.sl.rate)>=Number(this.market_state.bid)){result = 'c'}
    if (Number(this.market_state.ask) === 0 || Number(this.market_state.bid) === 0){result=false}
    this.ABCState=result
}
//расчёт tp sl
Deal.prototype.calcTPSL = function () {
    if (this.order.status !== 'executed_processed'){

    }
}


//ордер исполнен полностью
//      tp.amount=order.amount
//      sl.amount=order.amount

//ордер исполнен частично
//      tp.amount=order.startingAmount-order.amount




//написать watcher универсальный с заделом на margin trading (short/long)
//обвешать всё тестами грамотно!!!!!

module.exports = {Deal: Deal}

//тесты
var order_params={type:'buy',pair:'USDT_BTC',rate:10000,amount:0.01}
var tp_params={type:'sell',pair:'USDT_BTC',rate:11000,amount:0.01}
var sl_params={type:'sell',pair:'USDT_BTC',rate:9000,amount:0.01}

var deal_params={order:order_params,tp:tp_params,sl:sl_params}

var deal = new Deal(deal_params)

setInterval(function () {
    deal.processTPSL()
    // console.log(deal)
},1000)

