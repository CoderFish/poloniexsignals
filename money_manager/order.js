const uid = require('uuid/v4')
const l = require('../core/functions.js')
//тестовые заглушки
const ut = require('./unit.js')
const Queue = require('../core/queue.js')

//order statuses
//init               - ордер только инициализирован
//processing         - ордер в процессе размещения
//fail               - не удалось поставить ордер
//placed             - ордер размещён
//executed           - ордер выполнен
//executed_part      - ордер исполнен частично
//executed_processed - выполнен и обработан(пересчитывать tp,sl не нужно ставиться сделкой)
//canceled           - ордер отменён

function Order(params){
    this.inner_id=uid()
    this.deal=deal
    //основные параметры ордера
    if (!('type' in params.oparams) || !('pair' in params.oparams) || !('rate' in params.oparams) || !('amount' in params.oparams)){return {}}
    this.status = 'init'
    this.pair=params.oparams.pair
    this.type=params.oparams.type
    this.rate=params.oparams.rate
    this.amount=oparams.amount
    // this.exchange = params.exchange
    // this.queue = params.queue
    this.queue = new Queue.MM_Queue()
    //дополнительные параметры ордера
    if (params.oparams.fillOrKill){this.fillOrKill=params.oparams.fillOrKill}else{this.fillOrKill=false}
    if (params.oparams.immediateOrCancel){this.immediateOrCancel=params.oparams.immediateOrCancel}else{this.immediateOrCancel=false}
    if (params.oparams.postOnly){this.postOnly=params.oparams.postOnly}else{this.postOnly=false}
    //состояние после постановки ордера
    this.state={}
}

//ставит ордер buy/sell
Order.prototype.putOrder = function() {
    if (this.status === 'init'){
        this.status = 'processing'
        //тут заглушка
        new ut.ut_exchange()[this.type](this.pair,this.rate,this.amount,this.fillOrKill,this.immediateOrCancel,this.postOnly,this.cbPutOrder.bind(this),{success:true})
        // this.exchange[this.type](this.pair,this.rate,this.amount,this.fillOrKill,this.immediateOrCancel,this.postOnly,this.cbPutOrder.bind(this))
    }
}
Order.prototype.cbPutOrder = function (err,data) {
    if (err){this.status='fail';l.log(err);return}
    this.orderNumber=data.orderNumber
    this.status = 'placed'
    this.queue.addJob(this.checkOrder.bind(this))
}
Order.prototype.checkOrder = function () {
    //тут тоже заглушка
    // this.exchange.returnOpenOrders(this.pair,function (err,data) {
    new ut.ut_exchange().returnOpenOrders(this.pair,function (err,data) {
        if (err){l.log('Не смог получить данные об открытых ордерах '+err);return}
        let key
        let founded=false
        for (key in data){
            if (Number(data[key].orderNumber) === Number(this.orderNumber)){
                this.state=data[key]
                if (this.state.startingAmount !== this.state.amount){this.status='executed_part'}
                founded=true
                this.queue.addJob(this.checkOrder.bind(this))
                break
            }
        }
        if (!founded){if (this.status !== 'canceled'){this.status = 'executed'}}
    }.bind(this),{success:true,empty:false,part_executed:false})
}

//отменяет ордер
Order.prototype.cancelOrder = function() {
    if (!this.orderNumber){l.log('Не могу отменить ордер, отсутствует orderNumber');return}
    //тут заглушка
    new ut.ut_exchange().cancelOrder(this.orderNumber,this.cbCancelOrder.bind(this),{success:true})
    // this.exchange.cancelOrder(this.orderNumber,this.cbCancelOrder.bind(this))
}
Order.prototype.cbCancelOrder = function (err,data) {
    if (err){l.log(err);return}
    if (data.success){this.status = 'canceled';this.state={}}
}

//двигает ордер в нужное место
// Order.prototype.moveOrder = function() {return}

module.exports = {Order: Order}
