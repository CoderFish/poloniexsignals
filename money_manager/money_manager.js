const uid = require('uuid/v4')
var _ = require('lodash')
var fs = require('fs')
const l = require('../core/functions.js')
const Poloniex = require('poloniex-api-node')
const Queue = require('../core/queue.js')
const Deal = require('./deal.js')
const moment = require('moment')
const debug = true
/*
// const redis_serialize_key='money_manager.serialize'
// const control_channel='money_manager.control'
// const uniq_mm_key='1'//уникальный для каждого mm ключ
const redis_get = require("redis").createClient()
redis_get.on("error", function (err) {l.log("Error " + err)})
const redis_sub = require("redis").createClient()
redis_sub.on("error", function (err) {l.log("Error " + err)})
redis_sub.on("message", function (channel, message) {console.log(channel,JSON.parse(message))})
redis_sub.subscribe(control_channel)
*/

var trade_plan_params={
    mm_id:'some_uniq_id',//уникальный идентификатор экземпляра money_manager'а
    api:'key',
    secret:'secret',
    max_open_orders:3,//кол-во открытых ордеров
    tp_sl_ratio:3, //соотношение take_profit к stop_loss в данном случае 3 tk к 1 sl

    //в дальнейшем
    //step between order price and sl
    // ограничение кол-ва убыточных сделок
    // за период
}

var deal_params= {
    pair: 'USDT_BTC',//торгуемая пара
    order: {
        type: 'buy',//тип ордера
        price: '10000',//цена
        amount: '0.1'//кол-во
    },
    tp:{type:'fix',//фиксировный
        price:18000
    },
    sl:{type:'fix',
        price:9000
    }
}

var deal_params1={
    pair:'USDT_BTC',//торгуемая пара
    order:{type:'buy',//тип ордера
           price:'10000',//цена
           amount:'0.01'//кол-во
    },
    //варианты стопа тэйка
    tp:{type:'calc',//расчитать
        step:'1000',//шаг если нужно рассчитывать
    },
    sl:{type:'calc',
        step:'1000'
    }

    /*
    tp:{type:'fix',//фиксировный
        price:18000
    },
    sl:{type:'fix',
        price:9000
    }
    */

    //tp sl могут отсутствовать, учесть!!!

    //реализовать скользящий стоп
    //реализовать мониторинг толстого мешающего движению рынка
  // buy(currencyPair, rate, amount, fillOrKill, immediateOrCancel, postOnly [, callback])
  // sell(currencyPair, rate, amount, fillOrKill, immediateOrCancel, postOnly [, callback])
    // tp:[[undefined,0,null,false],number,'calc']
    // sl:[[undefined,0,null,false],number,'calc']
    // ssl:{//смещает заявку при определённых условиях
    //     candle_period:60*60*2  //каждые 2 часа проверяем  лоу
    //     low_step:number //и если он выше предыдущего самого высокого лоу смещаем на low_step от него
    // }
}

function MoneyMager(trade_plan) {
    this.trade_plan=trade_plan
    this.trade_plan.cur_open_orders=0
    this.deals={}
    this.balances={}
    this.cur_deal_speed=1000//скорость хода курсора
    this.return_balances_speed=3000
    this.cur_deal_id=false
    this.mm_cache_dir='./mm_cache/'
    this.fn_deal_serial=this.mm_cache_dir+this.trade_plan.mm_id+'.txt'
    this.poloniex = new Poloniex(this.trade_plan.api,this.trade_plan.secret,{ socketTimeout: 60000 , keepAlive: true}) //ключи вынести в бд
    this.poloniex.on('error', (error) => {l.log(error)})
    this.queue = new Queue.MM_Queue(2)
    this.deserealize()

    //ставим курсор на обход
    // setInterval(this.processNextDeal.bind(this),this.cur_deal_speed)
    //ставит задачу получить баланс в очередь так чтоб баланс и открытые ордера получались каждые this.return_balances_speed с высоким приоритетом
    // setInterval(function (context) {
    //     this.queue.addJobAtTime(context.returnCompleteBalances.bind(context),moment().unix()+1)
    //     this.queue.addJobAtTime(context.returnOpenOrders.bind(context),moment().unix()+1)
    // },this.return_balances_speed,this)

}

//десериализация/сеиализация открытых ордеров
MoneyMager.prototype.deserealize=function (){
    if (fs.existsSync(this.mm_cache_dir)!==false && fs.existsSync(this.fn_deal_serial)!==false){
        this.deals=JSON.parse(fs.readFileSync(this.fn_deal_serial, 'utf8'))
    }
}
MoneyMager.prototype.serialize=function (){
    if (fs.existsSync(this.mm_cache_dir)===false){fs.mkdirSync(this.mm_cache_dir)}
    fs.writeFileSync(this.fn_deal_serial, JSON.stringify(this.deals));
}

/*
//получаем баланс
MoneyMager.prototype.returnCompleteBalances = function () {this.poloniex.returnCompleteBalances('all',this.cbPrepareBalances.bind(this))}
//callBack для получения баланса
MoneyMager.prototype.cbPrepareBalances = function (err,data) {
    if (err){l.log('Ошибка при получении баланса '+err)}
    let key
    let balances ={}
    for (key in data){if (!(Number(data[key].available) === 0 && Number(data[key].onOrders) === 0 && Number(data[key].btcValue) === 0)){balances[key]=data[key]}}
    this.balances = balances
    if (debug){l.log('Баланс получен '+JSON.stringify(this.balances))}
}
//получаем открытые сделки
MoneyMager.prototype.returnOpenOrders = function () {this.poloniex.returnOpenOrders('all',this.cbPrepareOpenOrders.bind(this))}
//callBack для получения открытых сделок
MoneyMager.prototype.cbPrepareOpenOrders = function (err,data) {
    if (err){l.log('Ошибка при получении баланса '+err)}
    let key
    let open_orders={}
    for (key in data){
        if (data[key].length !== 0){
            open_orders[key]=data[key]
        }
    }


///тут должно что тобыть
    console.log(open_orders)
}
*/


//пробуем стартануть сделку
MoneyMager.prototype.startDeal=function(params){
    if (!this.canPostOrder()) {
        if (debug){l.log('Достигнут предел открытых позиций '+JSON.stringify(params))}
        return false
    }
    if (debug){l.log('Добавлен ордер на исполнение '+JSON.stringify(params))}
    this.putDeal(params)
}
//проверка можно ли поставить ордер
MoneyMager.prototype.canPostOrder=function(){
    if (this.trade_plan.max_open_orders>this.trade_plan.cur_open_orders){
        this.trade_plan.cur_open_orders++
        return true
    }
    return false
}
//инициализируем и кладём в массив параметры сделки
MoneyMager.prototype.putDeal=function(params){
    params.exchange = this.poloniex
    params.queue = this.queue
    let deal = new Deal.Deal(params)
    this.deals[deal.id]=deal
}


//тестовые методы
MoneyMager.prototype.processDeal = function(){
    //нужно обработать конкретную сделку исходя из курсора
    let key
    for (key in this.deals){break}
    this.deals[key].processDeal()
}
MoneyMager.prototype.cancelOrder = function(){
    //нужно обработать конкретную сделку исходя из курсора
    let key
    for (key in this.deals){break}
    let params = {exchange:this.poloniex}
    this.deals[key].order.cancelOrder(params)
}


/*
//курсор обрабатывет ордера
MoneyMager.prototype.processNextDeal=function(){

    //обработка сделок

// console.log(this.cur_deal_id)

    //обработка текущего ордера
    this.processOrder(
        function () {
            //если нужно удалить сделку из массива откладывааем пока не посчитаем следующую
            this.getNextDealID()
            console.log('hit')
            //отложенное решение удалить сделку из массива если нужно
            //сериализация по итогу
        }
    )
}
//обработка текущего ордера
MoneyMager.prototype.processOrder=function(callBack){

    if (!this.deals[this.cur_deal_id].order.placed){
        if (this.deals[this.cur_deal_id].order.type === 'buy' ||
            this.deals[this.cur_deal_id].order.type === 'sell')
        {
            if (debug){l.log('Ставлю ордер '+this.deals[this.cur_deal_id].order.type+' на исполнение '+JSON.stringify(this.deals[this.cur_deal_id]))}
            // if (debug){l.log('Ставлю ордер')}
            this.deals[this.cur_deal_id].order.placed='processing...'

            // this.cbProcessOrder=callBack
            // this.poloniex[this.deals[this.cur_deal_id].order.type](
            //     this.deals[this.cur_deal_id].pair,
            //     this.deals[this.cur_deal_id].order.price,
            //     this.deals[this.cur_deal_id].order.amount,
            //     this.deals[this.cur_deal_id].order.fillOrKill,
            //     this.deals[this.cur_deal_id].order.immediateOrCancel,
            //     this.deals[this.cur_deal_id].order.postOnly,
            //     this.cbOrderPlaced.bind(this)
            // )
        }
    }else{
        // if (debug){
        //     l.log('Состояние заявки id:'+this.cur_deal_id+' '+this.deals[this.cur_deal_id].order.placed)
        // }
    }
}
//калбэк к поставленному ордеру
MoneyMager.prototype.cbOrderPlaced=function (err,data) {
    // console.log('++++++',this.cur_deal_id)
    // console.log(5555,this)

    this.cbProcessOrder()

    // if (err !== null){
    //     console.log(err,data);
    //     this.deals[this.cur_deal_id].order.placed='fail';
    //     return
    // }
    // data.order.placed=true
    //и тут уже ставим sl и tp
    // после
    // console.log('++++++++++',err,'-------',data)
}
//Устанавливает id следующей сделки
MoneyMager.prototype.getNextDealID=function(){
    let keys = _.keys(this.deals)
    if (keys.length!==0) {
        //если не указана текущая сделка ставыим первую
        if (this.cur_deal_id === false) {this.cur_deal_id = _.head(keys);return}




        let next = false
        for (key in this.deals) {
            if (key === this.cur_deal_id) {
                if (this.cur_deal_id === _.findLastKey(this.deals)) {
                    this.cur_deal_id = false;
                    break
                } else {
                    next = true
                }
            } else {
                if (next) {
                    this.cur_deal_id = key;
                    break
                }
            }
        }
    }else{if(debug)(l.log('не чего выполнять, активных сделок нет'))}
}

/*
class MoneyMager{
    // исходя из торговой стратегии решает задачи по откртию сделки
    // контролю за сделками
    // сериализация и десериализация данных открытых сделок
    constructor(trade_plan) {

        this.deserealize(function (data,cont) {
            console.log(data)
            setInterval(function (context) {context.processNextDeal()},1000,cont)
            //курсор по сделкам
        },this)

        //дефолтные настройки
        //совмещаем params c дефолтными настройками
        //создаём объект poloniex
        //садимся на управляющий канал в редис

    }

    //пробует добавить ордер исходя из торгового плана
    //сериализация открытых ордеров

    serialize (){
        // this.redis_get.set(redis_serialize_key+'.'+this.trade_plan.mm_id,JSON.stringify(this.deals))
    }

    //десериализация открытых ордеров
    deserealize (callBack,cont){
        // var promise = new Promise((resolve, reject) => {
        //     this.redis_get.get(redis_serialize_key+'.'+this.trade_plan.mm_id, function(err, data) {
        //         if (err) {l.log(err);return}
        //         if (data) {data=JSON.parse(data)} else {data=null;l.log('Нет данных!')}
        //         resolve(data)
        //     })
        // }).then(data => {callBack(data,cont)})
    }

    //обработать следующую сделку
    processNextDeal(){

        //обрабатывает следующую сделку по курсору
        //добывает инфу по тек. сделке,
        //снимает или ставит заявки или удаляет себя из курсора
        if (this.deals.length !== 0){
            let keys = _.keys(this.deals)
            if (this.cur_deal_id === false ){this.cur_deal_id=_.head(keys)}
            //далее манипуляции с данными


             // console.log(_.nth(keys,1))



            //расчёт нового cur_deal_id
            // console.log(this.cur_deal_id)
        }else{console.log('не чего выполнять, активных сделок нет')}
    }
    //создаёт структуру и кладёт в массив

}
/*
class Deal {

    // cancelDealOrders(){
    // отменяет все сл и тп
    // }

    //при исполнении тп или сл снимаем противоположную
        //храним состояние сделки
        //{стоит ли тэйк и лось если они нужны,}
    //соответственно
    // constructor(params){
        //сохраняем параметры
    // }
    //public functions buy(params){
        //кидает заявку на 1 шаг выше
        //слушает стакан если появляетсзаявка выше моей и висит 10 сек снимаем заявку ставим на 1 шаг выше
        //каждые const сек проверяем исполненность заявки
        //если заявка исполнена рассчитываем sl и tk если нужно
    // }
    // sell(params){}
    // moveOrder(params){}
    // cancelOrder(params){}


    //private functions
    // returnBalances() {}
    // returnCompleteBalances() {}
    // returnOpenOrders() {}

    // buy() {}
    // sell() {}
    // cancelOrder() {}
    // moveOrder() {}

    //?
    // returnFeeInfo() {}
    // returnTradableBalances() {}



    // returnDepositAddresses
    // generateNewAddress
    // returnDepositsWithdrawals
    // returnTradeHistory
    // returnOrderTrades
    // withdraw
    // returnAvailableAccountBalances
    // transferBalance
    // returnMarginAccountSummary
    // marginBuy
    // marginSell
    // getMarginPosition
    // closeMarginPosition
    // createLoanOffer
    // cancelLoanOffer
    // returnOpenLoanOffers
    // returnActiveLoans
    // returnLendingHistory
    // toggleAutoRenew
}
*/

var mm = new MoneyMager(trade_plan_params)

var order_params={type:'buy',pair:'USDT_BTC',rate:10000,amount:0.01}
var deal_params={order:order_params}

mm.startDeal(deal_params)

mm.processDeal()

setInterval(function () {
    // console.log(mm.deals)
},100)


setTimeout(function () {
  let key
  for (key in mm.deals){
    mm.deals[key].order.cancelOrder()
  }
},2000)
