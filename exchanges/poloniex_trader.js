/****************************************************************************
 * Возвращает в редис данные в каналы с префиксом exchange.poloniex.
 *              если инфа по паре то это exchange.poloniex.pair.[USDT_BTC]
 * Подписан на управляющий канал poloniex.control
 ***************************************************************************/

const debug = true
const l = require('../core/functions.js')
const moment = require("moment")
var Config = require('../config.js')
var config = new Config.ExchangesConfig()
const redis_sub = require("redis").createClient(config.redis)
const redis_pub = require("redis").createClient(config.redis)
const Poloniex = require('poloniex-api-node')
const ctrl_channel='poloniex.trader.control'
const channel_out_prefix='exchange.poloniex.trader'
const req_in_sec=4
let poloniex = new Poloniex('API','SECRET',{ socketTimeout: 60000 , keepAlive: true})

redis_pub.on("error", function (err) {l.log('Error redis_pub: ' + err)})
redis_sub.on("error", function (err) {l.log('Error redis_sub: ' + err)})
poloniex.on('error', function (err) {l.log('Poloniex error: ' + err)})
poloniex.on('close', function(){l.log('WebSocket connection closed, try to reconnect...')})

var api_access_dt={ts:0,cnt:0}

//REST API
redis_sub.on("message", function (channel, data) {
  if (channel===ctrl_channel) {
    var data = JSON.parse(data)
    if (debug) {l.log('redis получил запрос на управляющий канал: '+data.command)}
    var f=function(data){
      //сперва проверяем есть ли ключ в редис channel_out_prefix+'.'+data.command
      var promise = new Promise((resolve, reject) => {
        redis_get.get(channel_out_prefix+'.'+data.command, function(err, val) {
        if (err) {l.log(err);return}
        if (val === null){reject('not_found')}else{resolve(val)}
      })
    }).then(val => {//если в редисе что то было возвращаем
        redis_pub.publish(channel_out_prefix+'.'+data.command,val)
      if (debug) {l.log('redis вернул данные в qканал '+channel_out_prefix+'.returnTradeHistory')}
    },
      not_found => {//вот эт месрасширять API функциями
        var f_command = function(pair,command,data,err){
          redis_pub.publish(channel_out_prefix+'.'+command+'.'+pair,JSON.stringify({pair :pair,data:data}))
          redis_pub.set(channel_out_prefix+'.'+command+'.'+pair,JSON.stringify({pair :pair,data:data}),'EX',1)
          if (debug) {l.log('poloniex вернул данные в канал '+channel_out_prefix+'.'+command+'.'+pair)}
        }
        if (data.command==='returnBalances'){poloniex.returnBalances().then((balances,err) => {f_command('all','returnBalances',balances,err)}).catch((err) => {l.log(err.message)})}else
        if (data.command==='returnCompleteBalances'){poloniex.returnCompleteBalances().then((balances,err) => {f_command('all','returnCompleteBalances',balances,err)}).catch((err) => {l.log(err.message)})}else
        if (data.command==='returnOpenOrders'){poloniex.returnOpenOrders(data.pair).then((balances) => {f_command(data.pair,'returnOpenOrders',balances)}).catch((err) => {l.log(err.message)})}
      })
    }
    var tf=function(data){if (canMakeRequest(true)){f(data)}else{setTimeout(function(val){tf(val)},1000,data)}}
    tf(data)
  }
})

// poloniex.openWebSocket({ version: 2 })
redis_sub.subscribe(ctrl_channel)
l.log('redis подписался на управляющий канал '+ctrl_channel)

function canMakeRequest(increase){
  if (api_access_dt.ts < moment().unix()) {api_access_dt.ts = moment().unix();api_access_dt.cnt=0}
  if (increase) {api_access_dt.cnt=api_access_dt.cnt+1}
  return api_access_dt.cnt<req_in_sec
}
