/****************************************************************************
 * Возвращает в редис данные в каналы с префиксом exchange.poloniex.
 *              если инфа по паре то это exchange.poloniex.pair.[USDT_BTC]
 * Подписан на управляющий канал poloniex.control
 ***************************************************************************/

const debug = true
const l = require('../core/functions.js')
const moment = require("moment")
var Config = require('../config.js')
var config = new Config.ExchangesConfig()
const redis_sub = require("redis").createClient(config.redis)
const redis_pub = require("redis").createClient(config.redis)
const redis_get = require("redis").createClient(config.redis)
const Poloniex = require('poloniex-api-node')
const ctrl_channel='poloniex.control'
const channel_out_prefix='exchange.poloniex'
const req_in_sec=4
let poloniex = new Poloniex({ socketTimeout: 60000 , keepAlive: true})

redis_pub.on("error", function (err) {l.log('Error redis_pub: ' + err)})
redis_sub.on("error", function (err) {l.log('Error redis_sub: ' + err)})
redis_get.on("error", function (err) {l.log('Error redis_get: ' + err)})
poloniex.on('error', function (err) {l.log('Poloniex error: ' + err)})
poloniex.on('close', function(){l.log('WebSocket connection closed, try to reconnect...');this.openWebSocket({ version: 2 })})

var api_access_dt={ts:0,cnt:0}

poloniex.on('open', function() {
    //подписываемся на пары
    for (key in config.data.poloniex.pairs){poloniex.subscribe(key);l.log('poloniex подписался на пару: '+key)}
    l.log('Ебашу и жду приказов!')
})

//данные из WebSocket
poloniex.on('message', (channelName, data) => {
    //Подписка на онлайн инфу по паре
    if (channelName.indexOf('_')>-1){redis_pub.publish(channel_out_prefix+'.pair.'+channelName,JSON.stringify({pair :channelName,data:data}))}
})

//REST API
redis_sub.on("message", function (channel, data) {
    if (channel===ctrl_channel) {
        var data = JSON.parse(data)
        if (debug) {l.log('redis получил запрос на управляющий канал: '+data.command)}
        var f=function(data){
            //сперва проверяем есть ли ключ в редис channel_out_prefix+'.'+data.command
            var promise = new Promise((resolve, reject) => {
                redis_get.get(channel_out_prefix+'.'+data.command, function(err, val) {
                    if (err) {l.log(err);return}
                    if (val === null){reject('not_found')}else{resolve(val)}
                })
            }).then(val => {//если в редисе что то было возвращаем
                        redis_pub.publish(channel_out_prefix+'.'+data.command,val)
                        if (debug) {l.log('redis вернул данные в канал '+channel_out_prefix+'.returnTradeHistory')}
                    },
                    not_found => {//вот эт месрасширять API функциями
                            var f_command = function(pair,command,data,err){
                                if (err) {l.log(err);return}
                                redis_pub.publish(channel_out_prefix+'.'+command+'.'+pair,JSON.stringify({pair :pair,data:data}))
                                redis_pub.set(channel_out_prefix+'.'+command+'.'+pair,JSON.stringify({pair :pair,data:data}),'EX',1)
                                if (debug) {l.log('poloniex вернул данные в канал '+channel_out_prefix+'.'+command+'.'+pair)}
                            }
                            if (data.command==='returnChartData'){
                                poloniex.returnChartData(data.pair, data.period, data.start, data.end , function (err,pdata) {
                                    f_command(data.pair,'returnChartData',pdata,err)
                                })
                            }else
                            if (data.command==='returnTradeHistory'){
                                poloniex.returnTradeHistory(data.pair, data.start, data.end , data.limit, function (err,pdata) {
                                    f_command(data.pair,'returnTradeHistory',pdata,err)
                                })
                            }else
                            if (data.command==='returnOrderBook'){
                                poloniex.returnOrderBook(data.pair, data.depth, function (err,pdata) {
                                    f_command(data.pair,'returnOrderBook',pdata,err)
                            })
                        }
                    }
            )
        }
        var tf=function(data){if (canMakeRequest(true)){f(data)}else{setTimeout(function(val){tf(val)},1000,data)}}
        tf(data)
    }
})

poloniex.openWebSocket({ version: 2 })
redis_sub.subscribe(ctrl_channel)
l.log('redis подписался на управляющий канал '+ctrl_channel)

function canMakeRequest(increase){
    if (api_access_dt.ts < moment().unix()) {api_access_dt.ts = moment().unix();api_access_dt.cnt=0}
    if (increase) {api_access_dt.cnt=api_access_dt.cnt+1}
    return api_access_dt.cnt<req_in_sec
}