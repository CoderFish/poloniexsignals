/*****************************************************
    Подписываается на на всё с полоникса
    конвертит варит, нужную дату публикует в редис
    готовит orderBook, tradeHistory (период?)

    на выходе в
        converted.poloniex.trade_hist.[pair] должна лежать история сделок
        converted.poloniex.order_book.[pair] должен лежать OrderBook
 *****************************************************/
const debug = true
const channel_out_prefix='exchange.poloniex'
const ctrl_channel='poloniex.control'
const l = require('../core/functions.js')
const moment = require("moment")
const cursor_upd_time=3
var Config = require('../config.js')
config = new Config.ExchangesConfig()

const redis_sub = require("redis").createClient(config.redis)
redis_sub.on("error", function (err) {l.log("Error redis_sub: " + err)})
const redis_pub = require("redis").createClient(config.redis)
redis_pub.on("error", function (err) {l.log("Error redis_pub: " + err)})
const redis_get = require("redis").createClient(config.redis)
redis_get.on("error", function (err) {l.log("Error redis_get: " + err)})

var pairs={}

    function makePairBlank(pair) {if (pairs[pair] === undefined){pairs[pair]={trade_hist:null,order_book:null}}}
    redis_sub.on("pmessage", function (channel_mask, channel, data) {
        channel=channel.split('.')
        data=JSON.parse(data)
        if (channel[2]==='pair'){
            //подписка
            makePairBlank(channel[3])
            for (key in data.data){
                if (data.data[key].type === 'orderBookRemove'){removeFromOrderBook(channel[3],data.data[key])}else
                if (data.data[key].type === 'orderBookModify'){modifyOrderBook(channel[3],data.data[key])}else
                if (data.data[key].type === 'newTrade'){processTradeHist(channel[3],Array(data.data[key].data))}
            }
        }
        if (channel[2]==='trader'){
          if (channel[3]==='returnOpenOrders'){
            redis_get.set('converted.poloniex.trader.returnOpenOrders.'+data.pair,JSON.stringify(data.data),'EX',10)
// console.log(data.data)
          }else
          if (channel[3]==='returnCompleteBalances'){
            let balances=getCleanCompleteBalances(data.data)
            redis_get.set('converted.poloniex.trader.returnCompleteBalances',JSON.stringify(balances),'EX',10)
            redis_pub.publish('converted.poloniex.trader.returnCompleteBalances',JSON.stringify(balances))
          }else
          if (channel[3]==='returnBalances'){
            let balances=getCleanBalances(data.data)
            redis_get.set('converted.poloniex.trader.returnBalances',JSON.stringify(balances),'EX',10)
            redis_pub.publish('converted.poloniex.trader.returnBalances',JSON.stringify(balances))
          }
        }else{
          //запрос к REST API
          makePairBlank(channel[3])
          if (channel[2]==='returnTradeHistory'){processTradeHist(channel[3],data.data)}
          if (channel[2]==='returnOrderBook'){prepareOrderBook(channel[3],data.data)}
        }
    })

    //подписываемся на всё
    redis_sub.psubscribe("exchange.poloniex.*")

    //курсор нужен чтоб ходить каждые cursor_upd_time сек по парам делать запросы к redis/poloniex, чтобы держать информацию актуальной
    var getNextPair = function(pair,is_first){
        var i=0
        for (key in config.data['poloniex'].pairs){i++}
        var pairs_length=i
        //расчёт следующей пары
        if (!is_first){
            i=0
            var first
            var next=false
            for (key in config.data['poloniex'].pairs){
                i++
                if (i === 1){first=key}
                if (next){pair=key;break}
                if (key === pair){if (pairs_length === i){pair=first;break}else{next=true}}
            }
        }
        return pair
    }
    //trade_hist_cursor
    var cursor_trade_hist = function (pair,is_first){
        pair=getNextPair(pair,is_first)
        //рекусивный вызов курсора
        setTimeout(function (next_pair) {
            var data = {
                command:'returnTradeHistory',
                pair   :pair,
                start  :moment().unix()-config.data['poloniex'].pairs[pair].trade_hist_period,
                end    :moment().unix(),
                limit  :0
            }
            redis_pub.publish(ctrl_channel,JSON.stringify(data))
            cursor_trade_hist(next_pair,false)
        },cursor_upd_time*1000,pair)
    }
    //запуск с первого элемента
    for (key in config.data['poloniex'].pairs){cursor_trade_hist(key,true);break}
    //order_book_cursor
    var cursor_order_book = function (pair,is_first){
        pair=getNextPair(pair,is_first)
        var inc = 1 //сперва получаем данные быстро, потом уменьшаем скорость в 10 раз
        if (!(pairs[pair] === undefined || pairs[pair].order_book === undefined || pairs[pair].order_book === null)){inc=10}
        //рекусивный вызов курсора
        setTimeout(function (next_pair) {
            var data = {
                command:'returnOrderBook',
                pair   :pair,
                depth  :config.data.poloniex.pairs[pair].order_book_depth
            }
            redis_pub.publish(ctrl_channel,JSON.stringify(data))
            cursor_order_book(next_pair,false)
        },cursor_upd_time*1000*inc,pair)//обходим order_book в 10 раз дольше чем cursor_upd_time
    }
    //запуск с первого элемента
    for (key in config.data['poloniex'].pairs){cursor_order_book(key,true);break}


    function getCleanCompleteBalances(data){
      let result={}
      for (key in data){if (Number(data[key].available) != 0 || Number(data[key].onOrders) != 0 || Number(data[key].btcValue) != 0) {result[key]=data[key]}}
      return result
    }

    function getCleanBalances(data){
      let result={}
      for (key in data){if (Number(data[key])!=0){result[key]=data[key]}}
      return result
    }

    function processTradeHist(pair,trade_hist){
        //установка 1 значения
        var setOneTrade = function(rec){
            let tradeID = rec.tradeID
            delete rec.tradeID
            rec.date=moment(rec.date).unix()
            pairs[pair].trade_hist['s'+tradeID]=rec
        }
        //установка начальных значений
        if (pairs[pair].trade_hist === null){
            pairs[pair].trade_hist={}
            for (key in trade_hist){setOneTrade(trade_hist[key])}
            return
        }
        //чистим старые данные
        for (key in pairs[pair].trade_hist){
            let ts = -1*(pairs[pair].trade_hist[key].date-moment().unix())-config.data.poloniex.exchange_time_shift
            if (Number(ts)>Number(config.data['poloniex'].pairs[pair].trade_hist_period)){delete pairs[pair].trade_hist[key]}
        }
        // добавляем новые данные
        for (key in trade_hist){setOneTrade(trade_hist[key])}

        // if (debug){
            // мониторинг кол-ва сторк в трэйд хист
            // let cnt=0
            // for (key in pairs[pair].trade_hist){cnt++}
            // console.log(pair,cnt,'trade_hist')
        // }
    }

    function prepareOrderBook(pair,data) {
        let result = {asks:{},bids:{}}
        for (aob in result){for (key in data[aob]){result[aob][data[aob][key][0]]=data[aob][key][1]}}
        pairs[pair].order_book=result
    }

    function removeFromOrderBook(pair,data) {
        if (pairs[pair].order_book!==null){
            if (pairs[pair].order_book[data.data.type+'s'][data.data.rate] !== undefined){
                delete pairs[pair].order_book[data.data.type+'s'][data.data.rate]
            }
        }
    }

    function modifyOrderBook(pair,data){if (pairs[pair].order_book!==null) {pairs[pair].order_book[data.data.type+'s'][data.data.rate]=data.data.amount}}

//откидываем trade_hist каждую секунду
setInterval(function () {for (key in pairs){redis_get.set('converted.poloniex.trade_hist.'+key,JSON.stringify(pairs[key].trade_hist),'EX',10)}},1000)
//откидываем order_book каждую секунду
setInterval(function () {for (key in pairs){redis_get.set('converted.poloniex.order_book.'+key,JSON.stringify(pairs[key].order_book),'EX',10)}},1000)


l.log('Работаю!')
