# Скрипты для отслеживания активности на бирже криптовалют [poloniex.com](https://poloniex.com/exchange)

Скрипты запускаются каким-либо мэнеджером процессов (в моём случае это pm2)

Обмен данными между скриптами и результат работы пишется в redis и в каналы pub/sub redis'а

Так же скрипты висят на управляющем канале, от которого получают инструкции

Для работы нужен [poloniex-api-node](https://www.npmjs.com/package/poloniex-api-node)

## Scripts

| Script                        | Comment |
| :---------------------------- | ---------------------------------------------------------------- |
| exchanges/poloniex.js         | получает и подписывается на обновления TradeHistory и OrderBook |
| exchanges/poloniex_conv.js    | конвертирует данные полученные от poloniex.js в удобный формат |
| exchanges/poloniex_trader.js  | скрипт для операций со счётом (операции со счётом реализованы на Go в другом проекте) |
| signals/signals.js            | генерирует сигнал на основе данных poloniex_conv.js |
| core/telegram/telegram_bot.js | телеграм бот транслирует сообщения из редиса |
| core/queue.js                 | In Memory очередь для исполнения команд с заданным интервалом времени |
| bots/enter_signal_bot.js      | принимает сигнал и решает что с ним делать (в данном случае если выполнено условие раз в 10мин шлёт сообщение в телеграм) |
| money_manager/                | папка где должен был быть мэнеджер сделок, но я отказался от его реализации на NodeJS и реализовал его на GoLang |

### BenchMark

При полной загрузке всех криптопар съедает примерно 40-60% от 2-х ядер i5 и 1.2gb оперативки, период TradeHistory 1ч, при этом скрипты постоянно шлют команды на получение TradeHistory и OrderBook не смотря на то что сидят на подписке, это сделано для того, что-бы гарантировано иметь актуальную информацию для расчётов

### Что дальше?

Дальнейшее развитие проекта я вижу в написании различных сигналов анализа активности в OrderBook и TradeHistory, типа появления "толстых" заявок на куплю/продажу и их разбор, отслеживание активности чужих торговых роботов по косвенным признакам, сбор этой статистической информации в БД для дальнейшего анализа
