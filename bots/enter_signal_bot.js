const telegram_ctl_channel='telegrram.bot'
const l = require('../core/functions.js');
var Config = require('../config.js')
const moment = require("moment")
var config = new Config.ExchangesConfig()
const redis_sub = require("redis").createClient(config.redis)
redis_sub.on("error", function (err) {l.log("Error " + err)})
const redis_pub = require("redis").createClient(config.redis)
redis_pub.on("error", function (err) {l.log("Error " + err)})

const msg_repeat_time=60*10
var steem_dt=0
var strat_dt=0

redis_sub.on("message", function (channel, message) {
    channel=channel.split('.')
    message=JSON.parse(message)
    pair=channel[3]
    if (pair==='BTC_STRAT'){check(pair,message,0.0011,0.0020)}else
    if (pair==='USDT_BTC'){check(pair,message,8000,11000)}else
    // if (pair==='USDT_BTC'){check(pair,message,10300,11000)}else
    if (pair==='USDT_ETC'){check(pair,message,26,false)}else
    if (pair==='BTC_ETC'){check(pair,message,0.0023,false)}else
    if (pair==='BTC_GNO'){check(pair,message,0.024,false)}else
    if (pair==='BTC_BCY'){check(pair,message,0.00007,false)}else
    if (pair==='BTC_CLAM'){check(pair,message,0.0005,false)}
})


function check(pair,message,low,high){
    let msg={}
    if (high && (+message.ask >= +high)){msg.data=pair+' подрос до '+message.ask}
    if (low && (+message.bid <= +low)){msg.data=pair+' упал до '+message.bid}
    if (msg.data){
        msg.data+='\n'+'vol: '+message.vol+'\n'+
                       'high: '+message.high+'\n'+
                       'low: '+message.low+'\n'+
                       'ask: '+message.ask+'\n'+
                       'bid: '+message.bid+'\n'+
                       'spread: '+message.spread+'\n'+
                       'buy_cnt: '+message.over_bs.buy_cnt+'\n'+
                       'sell_cnt: '+message.over_bs.sell_cnt+'\n'+
                       'buy_vol: '+message.over_bs.buy_vol+'\n'+
                       'sell_vol: '+message.over_bs.sell_vol
        let dt=moment().unix()
        if ((+strat_dt+msg_repeat_time)<=dt){redis_pub.publish(telegram_ctl_channel,JSON.stringify(msg));strat_dt=dt}
    }
}

function ss(channel){redis_sub.subscribe(channel);l.log('Подписался на канал: '+channel)}

// ss('signal.poloniex.market_state.BTC_STRAT')
ss('signal.poloniex.market_state.USDT_BTC')
// ss('signal.poloniex.market_state.USDT_ETC')
// ss('signal.poloniex.market_state.BTC_ETC')
// ss('signal.poloniex.market_state.BTC_GNO')
// ss('signal.poloniex.market_state.BTC_BCY')
// ss('signal.poloniex.market_state.BTC_CLAM')

l.log('Работаю!')
