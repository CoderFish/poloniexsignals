const moment = require("moment")
const l = require('./functions.js')
const debug = true

//очередь
function MM_Queue(q_speed) {
    if (q_speed){this.q_speed=q_speed}else{this.q_speed=1}//def 1s
    this.queue={}
    setInterval(this.runJob.bind(this),this.q_speed*1000)
}

//добавляет в конец очереди задачу
MM_Queue.prototype.addJob=function (funct) {
    let key
    let max_key
    for (key in this.queue){if (max_key === undefined || Number(key)>Number(max_key)) {max_key=key}}
    if (max_key === undefined){max_key=moment().unix()} else {max_key=Number(max_key)+Number(this.q_speed)}
    this.queue[max_key]={funct:funct}
}

//постановит задачу в нужное время выполнения сдвигая если нужно все остальные задачи
MM_Queue.prototype.addJobAtTime=function (funct,ts) {
    let key
    let q = {}
    for (key in this.queue){if (Number(key)>=Number(ts)) {q[Number(key)+Number(this.q_speed)]=this.queue[key]}else{q[key]=this.queue[key]}}
    q[ts]={funct:funct}
    this.queue = q
}

//запускает текущую задачу
MM_Queue.prototype.runJob=function () {
    let key
    for (key in this.queue){if (moment().unix()>=Number(key)){
        this.queue[key].funct();
        delete this.queue[key]};
        break
    }
}

module.exports = {MM_Queue: MM_Queue}