const moment = require("moment")

function log(msg){console.log('['+moment().format('DD-MM-YYYY HH:mm:ss')+'] '+msg)}

module.exports = {
    log: log
};
