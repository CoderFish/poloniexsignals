/********************************************************************
 * Класс реализует единый API к разным биржам
 ********************************************************************/
var l = require('./functions.js')
var Config = require('./config.js')
const redis_pub = require("redis").createClient()
redis_pub.on("error", function (err) {l.log('Error: ' + err)})

class ExchangeProvider {
    constructor() {
        this.is_created=false
        this.config = new Config.ExchangesConfig()
        this.config.getData(function(provider){provider.is_created=true},this)
    }

    //REST API
    getChartData(exchange,req_params){
        this.runWait(exchange,req_params,'getChartData')
    }
    getTradeHistory(exchange,req_params){
        this.runWait(exchange,req_params,'getTradeHistory')
    }
    getOrderBook(exchange,req_params){
        this.runWait(exchange,req_params,'getOrderBook')
    }


    //private
    runWait(exchange,req_params,command){//публикуем в [exchange].control req_params и команду
        let params={
            context:this,
            exchange:exchange,
            req_params:req_params,
            cb:function(cbp){
                cbp.req_params.command=params.context.config.data[exchange].api[command]
                redis_pub.publish(exchange+'.control',JSON.stringify(cbp.req_params))
            }
        }
        var f = function (fparams){fparams.cb(fparams)}
        if (this.is_created){f(params)}else{setTimeout(f,500,params)}
    }
}

module.exports = {
    ExchangeProvider: ExchangeProvider
}
